import app from "./app.js";
import { sequelize } from "./src/database/database.js";
import "./src/models/Loteria.js";
import "./src/models/Rifa.js";
import "./src/models/Usuario.js";
import dotenv from "dotenv";
import { fnLoterias, fnRifas, fnUsuarios } from "./src/database/loadDB.js";

dotenv.config();

const PORT = process.env.PORT || 3001;
// cargar datos una vez al iniciar el servidor
(async () => {
	try {
		await sequelize.sync({ force: true });
		await fnLoterias();
		await fnUsuarios();
		await fnRifas();
		console.log("Datos cargados exitosamente.");
	} catch (error) {
		console.error("Error al cargar los datos:", error);
	}
})();

// iniciar server después de cargar los datos
app.listen(PORT, () => {
	console.log(`Server corriendo en puerto ${PORT}`);
});
