import { Router } from "express";
import {
	deleteRifa,
	getRifas,
	postRifa,
	putRifa,
} from "../controllers/rifas.controller.js";

const router = Router();

router.get("/rifas", getRifas);
router.post("/rifas", postRifa);
router.put("/rifas/:id", putRifa);
router.delete("/rifas/:id", deleteRifa);

export default router;
