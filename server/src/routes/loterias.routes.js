import { Router } from "express";
import {
  deleteLoteria,
  getLoterias,
  postLoteria,
  putLoteria,
} from "../controllers/loterias.controller.js";

const router = Router();

router.get("/loterias", getLoterias);
router.post("/loterias", postLoteria);
router.put("/loterias/:id", putLoteria);
router.delete("/loterias/:id", deleteLoteria);

export default router;
