import { Loteria } from "../models/Loteria.js";
import { Rifa } from "../models/Rifa.js";

export const getLoterias = async (req, res) => {
	try {
		const allLoterias = await Loteria.findAll({
			include: [
				{
					model: Rifa,
					as: "rifas",
				},
			],
		});
		res.json(allLoterias);
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const postLoteria = async (req, res) => {
	const { nombre, ciudad, imagen, url } = req.body;

	const errors = [];
	if (!nombre) errors.push("El nombre es obligatorio");
	if (!ciudad) errors.push("La ubicación es obligatoria");
	if (!imagen) errors.push("La imagen es obligatoria");
	if (!url) errors.push("La url es obligatoria");

	if (errors.length > 0) {
		return res.status(400).json({ errors });
	}

	try {
		const newLoteria = await Loteria.create({ nombre, ciudad, imagen, url });
		res
			.status(200)
			.json({ message: "Lotería creada correctamente", newLoteria });
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const putLoteria = async (req, res) => {
	const { id } = req.params;
	const { nombre, ciudad, imagen, url } = req.body;

	try {
		const loteria = await Loteria.findByPk(id);

		if (loteria.length == 0) {
			return res.status(404).json({ message: "Lotería no encontrada" });
		} else {
			Loteria.update(
				{
					nombre: nombre || loteria.nombre,
					ciudad: ciudad || loteria.ciudad,
					imagen: imagen || loteria.imagen,
					url: url || loteria.url,
				},
				{ where: { id: id } }
			);
		}

		res.json("¡Lotería actualizada correctamente!");
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const deleteLoteria = async (req, res) => {
	const { id } = req.params;

	try {
		const deleted = await Loteria.destroy({ where: { id } });

		if (deleted) {
			res.json({ message: "¡Lotería eliminada correctamente!" });
		} else {
			res.status(404).json({ message: "Lotería no encontrada" });
		}
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};
