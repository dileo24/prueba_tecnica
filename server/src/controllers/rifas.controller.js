import { Rifa } from "../models/Rifa.js";
import { Loteria } from "../models/Loteria.js";
import { Usuario } from "../models/Usuario.js";

export const getRifas = async (req, res) => {
	try {
		const allRifas = await Rifa.findAll({
			include: [
				{
					model: Loteria,
					as: "loteria",
				},
				{
					model: Usuario,
					as: "usuarios",
					through: { attributes: [] },
				},
			],
			attributes: { exclude: ["loteriaId", "usuarioId"] },
			order: [["valor", "ASC"]],
		});
		res.json(allRifas);
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const postRifa = async (req, res) => {
	const {
		nombre,
		valor,
		fecha_inicial,
		fecha_final,
		descripcion,
		imagen,
		loteriaId,
		usuarioId,
	} = req.body;

	const errors = [];
	if (!nombre)
		errors.push({ message: "¡El nombre es obligatorio!", input: "nombre" });
	if (!valor)
		errors.push({ message: "¡El valor es obligatorio!", input: "valor" });
	if (!fecha_inicial)
		errors.push({
			message: "¡La fecha inicial es obligatoria!",
			input: "fecha_inicial",
		});
	if (!fecha_final)
		errors.push({
			message: "¡La fecha final es obligatoria!",
			input: "fecha_final",
		});
	if (!descripcion)
		errors.push({
			message: "¡La descripcion es obligatoria!",
			input: "descripcion",
		});
	if (!descripcion)
		errors.push({
			message: "¡La imagen es obligatoria!",
			input: "imagen",
		});
	if (!loteriaId)
		errors.push({
			message: "¡Es necesario relacionar la rifa con una lotería!",
			input: "loteriaId",
		});

	if (errors.length > 0) {
		return res.status(400).json({ errors });
	}

	try {
		const newRifa = await Rifa.create({
			nombre,
			valor,
			fecha_inicial,
			fecha_final,
			descripcion,
			imagen,
			loteriaId,
		});
		await newRifa.addUsuario(usuarioId);
		res.status(200).json({ message: "Rifa creada correctamente", newRifa });
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const putRifa = async (req, res) => {
	const { id } = req.params;
	const {
		nombre,
		valor,
		fecha_inicial,
		fecha_final,
		descripcion,
		imagen,
		usuarioId,
		loteriaId
	} = req.body;

	try {
		const rifa = await Rifa.findByPk(id);

		if (!rifa) {
			return res.status(404).json({ message: "Rifa no encontrada" });
		} else {
			await rifa.update({
				nombre: nombre || rifa.nombre,
				valor: valor || rifa.valor,
				fecha_inicial: fecha_inicial || rifa.fecha_inicial,
				fecha_final: fecha_final || rifa.fecha_final,
				descripcion: descripcion || rifa.descripcion,
				imagen: imagen || rifa.imagen,
				loteriaId: loteriaId || rifa.loteriaId,
			});

			if (usuarioId) {
				await rifa.addUsuario(usuarioId);
				return res.json("¡Rifa comprada correctamente!");
			}
		}

		res.json("¡Rifa actualizada correctamente!");
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const deleteRifa = async (req, res) => {
	const { id } = req.params;

	try {
		const deleted = await Rifa.destroy({ where: { id } });

		if (deleted) {
			res.json({ message: "¡Rifa eliminada correctamente!" });
		} else {
			res.status(404).json({ message: "Rifa no encontrada" });
		}
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};
