import { Usuario } from "../models/Usuario.js";
import { Rifa } from "../models/Rifa.js";

export const getUsuarios = async (req, res) => {
	try {
		const allUsuarios = await Usuario.findAll({
			include: [
				{
					model: Rifa,
					as: "rifas",
					through: { attributes: [] },
				},
			],
			attributes: { exclude: ["usuarioId"] },
		});
		res.json(allUsuarios);
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};

export const postUsuario = async (req, res) => {
	const { nombre, num_tel, email, direccion, cantidadRifas } = req.body;

	const errors = [];
	if (!nombre)
		errors.push({ message: "¡El nombre es obligatorio!", input: "nombre" });
	if (!num_tel)
		errors.push({
			message: "¡El número de teléfono es obligatorio!",
			input: "num_tel",
		});
	if (!email)
		errors.push({
			message: "¡El email es obligatorio!",
			input: "email",
		});
	if (!direccion)
		errors.push({
			message: "¡La dirección es obligatoria!",
			input: "direccion",
		});

	if (errors.length > 0) {
		return res.status(400).json({ errors });
	}

	try {
		const newUsuario = await Usuario.create({
			nombre,
			num_tel,
			email,
			direccion,
			cantidadRifas,
		});
		res
			.status(200)
			.json({ message: "Usuario creada correctamente", newUsuario });
	} catch (error) {
		res.status(500).json({ message: error.message });
	}
};
