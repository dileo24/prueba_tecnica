import { readFile } from "fs/promises";
import { Rifa } from "../models/Rifa.js";
import { Loteria } from "../models/Loteria.js";
import { Usuario } from "../models/Usuario.js";

async function loadJson(filePath) {
	const data = await readFile(filePath, "utf8");
	return JSON.parse(data);
}

export async function fnLoterias() {
	const loterias = await loadJson("./src/jsons/loterias.json");
	for (const lot of loterias) {
		await Loteria.create({
			nombre: lot.nombre,
			ciudad: lot.ciudad,
			imagen: lot.imagen,
			url: lot.url,
		});
	}
}

export async function fnUsuarios() {
	const usuarios = await loadJson("./src/jsons/usuarios.json");
	for (const user of usuarios) {
		await Usuario.create({
			nombre: user.nombre,
			num_tel: user.num_tel,
			email: user.email,
			direccion: user.direccion,
			cantidadRifas: user.cantidadRifas,
		});
	}
}

export async function fnRifas() {
	const rifas = await loadJson("./src/jsons/rifas.json");
	for (const rif of rifas) {
		const createdRifa = await Rifa.create({
			nombre: rif.nombre,
			valor: rif.valor,
			fecha_inicial: rif.fecha_inicial,
			fecha_final: rif.fecha_final,
			descripcion: rif.descripcion,
			imagen: rif.imagen,
			loteriaId: rif.loteriaId,
			usuarioId: rif.usuarioId,
		});
		if (rif.usuarioId) {
			const usuario = await Usuario.findByPk(rif.usuarioId);
			if (usuario) {
				await usuario.addRifa(createdRifa);
			} else {
				console.error("No se encontró el usuario con id 1");
			}
		}
	}
}
