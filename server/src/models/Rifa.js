import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Usuario } from "./Usuario.js";

export const Rifa = sequelize.define(
	"rifas",
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		nombre: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		valor: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		fecha_inicial: {
		  type: DataTypes.STRING,
		  allowNull: false,
		},
		fecha_final: {
		  type: DataTypes.STRING,
		  allowNull: false,
		},
		descripcion: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		imagen: {
			type: DataTypes.TEXT,
			allowNull: false,
		},
	},
	{
		timestamps: false,
	}
);
