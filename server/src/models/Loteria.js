import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Rifa } from "./Rifa.js";

export const Loteria = sequelize.define(
	"loterias",
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		nombre: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		ciudad: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		imagen: {
			type: DataTypes.TEXT,
			allowNull: false,
		},
		url: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		timestamps: false,
	}
);

Loteria.hasMany(Rifa, {
	foreignKey: "loteriaId",
	sourceKey: "id",
});
Rifa.belongsTo(Loteria, {
	foreignKey: "loteriaId",
	targetKey: "id",
});
