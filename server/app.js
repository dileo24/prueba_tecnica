import express from "express";
import cors from "cors";
import loterias_routes from "./src/routes/loterias.routes.js";
import usuarios_routes from "./src/routes/usuarios.routes.js";
import rifas_routes from "./src/routes/rifas.routes.js";
import { Rifa } from "./src/models/Rifa.js";
import { Usuario } from "./src/models/Usuario.js";

const app = express();
app.use(cors());
app.use(express.json()); //permite que el servidor interprete objetos json, para los middlewares
app.use(loterias_routes);
app.use(usuarios_routes);
app.use(rifas_routes);

Usuario.belongsToMany(Rifa, {
	through: "UsuarioRifas",
	as: "rifas",
	foreignKey: "usuarioId",
});
Rifa.belongsToMany(Usuario, {
	through: "UsuarioRifas",
	as: "usuarios",
	foreignKey: "rifaId",
});

export default app;
