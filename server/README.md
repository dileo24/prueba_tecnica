# Backend

Backend de la Prueba Técnica

Primeros Pasos a realizar:

Paso 1:

Recordar ejecutar npm install, ya que el gitignore esta ignorando los paquetes(node_modules).

Paso 1:

En pgAdmin o SQLShell crear la base de datos con el comando

```
CREATE DATABASE prueba_tecnica;
```

Paso 3:

Crear el archivo .env

dentro de ella escribir las variables de entorno:

```
DB_USER=postgres
DB_PASSWORD= ******
DB_HOST=localhost
DB_PORT=3001
```

Recordar usar su propia contraseña de Postgres y algún puerto liberado.

Paso 5:

Inicializar el backend escribiendo en la terminal...

```
npm start
```
