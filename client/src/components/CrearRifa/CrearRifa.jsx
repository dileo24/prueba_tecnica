import React, { useEffect, useState } from 'react';
import axios from 'axios';
import "./CrearRifa.css";
import { useLocation, useNavigate } from 'react-router-dom'; 
import { apiRifas } from '../../services/rifas.services';
import Navbar from '../Navbar/Navbar';
import { apiLoterias } from '../../services/loterias.services';

const CrearRifa = () => {
  const [formData, setFormData] = useState({});
  const [loterias, setLoterias] = useState([]); 
  const navigate = useNavigate();
  const location = useLocation();
  const datosRifa = location.state && location.state.datosRifa;
  
  const formatDate = (dateString, accion) => {
    if( accion === 'R'){
      const [day, month, year] = dateString.split('/');
      return `${year}-${month}-${day}`;
    } else if(accion === 'E'){
      const [day, month, year] = dateString.split('-');
      return `${year}/${month}/${day}`;
    }
  };

  const getLoterias = async () => {
    try {
      const response = await apiLoterias.getLoterias();
      setLoterias(response);
    } catch (error) {
      console.error("Error fetching loterias:", error);
    }
  };

  useEffect(() => {
    getLoterias();
    if (datosRifa) {
      setFormData({
        ...datosRifa,
        loteriaId: datosRifa.loteria.id,
        fecha_inicial: formatDate(datosRifa.fecha_inicial, 'R'),
        fecha_final: formatDate(datosRifa.fecha_final, 'R')
      });
    }
  }, [datosRifa]);

  const DatosForm = [
    { label: "Nombre", name: "nombre", type: "text" },
    { label: "Descripción", name: "descripcion", type: "text" },
    { label: "Valor", name: "valor", type: "number" },
    { label: "Fecha inicial", name: "fecha_inicial", type: "date" },
    { label: "Fecha final", name: "fecha_final", type: "date" },
    { label: "URL Imagen", name: "imagen", type: "text" },
    { label: "Lotería", name: "loteriaId", type: "text" },
  ];

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmitCrearRifa = async (e) => {
    e.preventDefault();
    try {
      const formattedData = {
        ...formData,
        fecha_inicial: formatDate(formData.fecha_inicial, 'E'),
        fecha_final: formatDate(formData.fecha_final, 'E')
      };
  
      if (formData.id) {
        await apiRifas.updateRifa(formData.id, formattedData);
      } else {
        await apiRifas.createRifa(formattedData);
      }
      navigate("/");
    } catch (error) {
      console.error("Error al enviar los datos del formulario:", error);
    }
  };

  return (
    <div className='contenedorGeneral'>
      <Navbar /> 
      <div className="contenedor">
        <div className="divTitulo">
          <p className="title">{datosRifa ? 'Editar Rifa' : 'Crear Rifa'}</p>
        </div>
        <div className="divDetalle">
          <div className="form">
            <form onSubmit={handleSubmitCrearRifa}>
              {DatosForm.map((dato, index) => (
                <div key={index} className="inputDiv">
                  <label className="form-label">{dato.label}</label>
                  {dato.name === "loteriaId" ? (
                      <select
                      className="form-input"
                      name={dato.name}
                      value={formData.loteriaId || ""}
                      onChange={handleChange}
                      required
                    >
                      <option value="" hidden>Seleccionar...</option>
                      {loterias.map((loteria) => (
                        <option key={loteria.id} value={loteria.id}>
                          {loteria.nombre}
                        </option>
                      ))}
                    </select>
                    
                  ) : (
                    <input
                      type={dato.type}
                      className="form-input"
                      name={dato.name}
                      value={formData[dato.name] || ""}
                      onChange={handleChange}
                      required
                    />
                  )}
                </div>
              ))}
              <button type="submit" className="btn enviar">
                {datosRifa ? 'Editar Rifa' : 'Crear Rifa'}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CrearRifa;
