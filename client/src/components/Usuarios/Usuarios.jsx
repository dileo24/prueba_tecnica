import React, { useEffect, useState } from "react";
import Card from "../Card/Card";
import axios from "axios";
import gif from "../../assets/cargando.gif";
import "./Usuarios.css";
import { apiUsuarios } from "../../services/usuarios.services";

const Usuarios = () => {
	const [usuarios, setUsuarios] = useState([]);

	const getUsuarios = async () => {
		try {
			const response = await apiUsuarios.getUsuarios();
			setUsuarios(response);
		} catch (error) {
			console.error("Error fetching usuarios:", error);
		}
	};

	useEffect(() => {
		getUsuarios();
	}, []);

	return usuarios.length > 0 ? (
		<div className="container divCont">
			<p className="title_sec">Últimos Compradores</p>
			<div className="usuarios">
				{usuarios.map((usuario) => (
					<Card key={usuario.id} data={usuario} />
				))}
			</div>
		</div>
	) : (
		<div className="divUsers divCont">
			<p className="title_sec noUsers">Todavía no hay compradores...</p>
			<img src={gif} alt="Cargando..." className="gif" />
		</div>
	);
};

export default Usuarios;
