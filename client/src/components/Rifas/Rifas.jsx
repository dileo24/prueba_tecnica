import React, { useEffect, useState } from "react";
import "./Rifas.css";
import { useNavigate } from "react-router-dom";
import { apiRifas } from "../../services/rifas.services";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";

const Rifas = () => {
	const [rifas, setRifas] = useState([]);
	const [abrirModal, setAbrirModal] = useState(false);
	const [rifaInfo, setRifaInfo] = useState(null);
	const [formFields, setFormFields] = useState([]);
	const navigate = useNavigate();

	const getRifas = async () => {
		try {
			const response = await apiRifas.getRifas();
			setRifas(response);
		} catch (error) {
			console.error("Error fetching rifas:", error);
		}
	};

	useEffect(() => {
		getRifas();
	}, []);

	const handleAbrirModal = (rifa, fields) => {
		setRifaInfo(rifa);
		setFormFields(fields);
		setAbrirModal(true);
	};

	const handleCerrarModal = () => {
		setAbrirModal(false);
		setRifaInfo(null);
		setFormFields([]);
	};

	const handleCrearRifa = () => {
		navigate("/crear_rifa");
	};

	const handleDelete = async (id) => {
		try {
			const confirmDelete = window.confirm(
				"¿Estás seguro de borrar esta rifa?"
			);

			if (confirmDelete) {
				await apiRifas.deleteRifa(id);
				setRifas(rifas.filter((rifa) => rifa.id !== id));
			}
		} catch (error) {
			console.error("Error al borrar rifa:", error);
		}
	};

	return (
		<div className="container divRif">
			<button className="btn" onClick={handleCrearRifa}>
				Crear Rifa
			</button>
			<br />
			<br />
			<br />
			<p className="title_sec">Rifas</p>
			<div className="rifas">
				{rifas &&
					rifas.map((rifa) => (
						<Card
							key={rifa.id}
							data={rifa}
							comprarBtn={() =>
								handleAbrirModal(rifa, [
									{ label: "Nombre", name: "nombre", type: "text" },
									{ label: "Email", name: "email", type: "email" },
									{ label: "Teléfono", name: "num_tel", type: "tel" },
									{ label: "Dirección", name: "direccion", type: "text" },
									{
										label: "Cantidad de rifas a comprar",
										name: "cantidadRifas",
										type: "number",
									},
								])
							}
							eliminarBtn={() => handleDelete(rifa.id)}
						/>
					))}
			</div>
			<Modal
				abrirModal={abrirModal}
				handleCerrar={handleCerrarModal}
				rifa={rifaInfo}
				DatosForm={formFields}
			/>
		</div>
	);
};

export default Rifas;
