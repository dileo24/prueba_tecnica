import React from "react";
import "./Card.css";
import ticket from "../../assets/ticket.png";
import user from "../../assets/user.png";
import lapiz from "../../assets/lapiz.png";
import trash from "../../assets/trash.png";
import { useNavigate } from "react-router-dom";

const Card = ({ data, comprarBtn, eliminarBtn }) => {
	const navigate = useNavigate();

	const handleEditarRifa = () => {
		navigate("/crear_rifa", { state: { datosRifa: data } });
	};
	return (
		<div>
			{data.valor ? (
				<div className="card" key={data.id}>
					<div className="card-header">
						<img
							src={lapiz}
							alt="edit"
							className="icon"
							onClick={handleEditarRifa}
						/>
						<img
							src={trash}
							alt="delete"
							className="icon"
							onClick={eliminarBtn}
						/>
					</div>
					<img src={ticket} alt="ticket" className="ticket-img" />
					<div className="divImg">
						<img src={data.imagen} alt="premio" className="img" />
					</div>
					<div className="info-container">
						<p className="nombre">{data.nombre}</p>
						<p className="descripcion">{data.descripcion}</p>
						<p className="loteria_info">
							{data.loteria.nombre} ({data.loteria.ciudad})
						</p>
						<p className="valor">${data.valor}</p>
						<p className="fechas">
							{data.fecha_inicial} - {data.fecha_final}
						</p>
					</div>
					<button className="btn" onClick={() => comprarBtn(data)}>
						¡Comprar Rifa!
					</button>
				</div>
			) : data.email ? (
				<div className="card" key={data.id}>
					<div className="cant_rifas">
						<p>{data.cantidadRifas}</p>
						<img src={ticket} alt="ticket" className="ticket-img" />
					</div>
					<div className="userInfo">
						<img src={user} alt="userFoto" />
						<p className="userNombre">{data.nombre}</p>
						<p className="userEmail">{data.email}</p>
						<p className="userTelefono">{data.num_tel}</p>
						<p className="userDireccion">{data.direccion}</p>
					</div>
					<div className="compra">
						<p className="compraTitulo">Rifas Compradas</p>
						{data.rifas.length &&
							data.rifas.map((rifa) => (
								<div key={rifa.id} className="compraInfo">
									<img
										src={rifa.imagen}
										alt={rifa.nombre}
										className="compraImg"
									/>
									<div>
										<p className="compraNombre">{rifa.nombre}</p>
										<p className="compraDescripcion">{rifa.descripcion}</p>
										<p className="compraFechas">
											{rifa.fecha_inicial} - {rifa.fecha_final}
										</p>
									</div>
								</div>
							))}
					</div>
				</div>
			) : (
				<div className="card" key={data.id}>
					<div className="cant_rifas">
						<p>{data.rifas.length}</p>
						<img src={ticket} alt="ticket" className="ticket-img" />
					</div>
					<div className="divImgLote">
						<img src={data.imagen} alt="loteria" className="imgLote" />
					</div>
					<div className="info-container">
						<p className="nombre">{data.nombre}</p>
						<p className="ciudad">{data.ciudad}</p>
					</div>
					<a
						href={data.url}
						target="_blank"
						rel="noopener noreferrer"
						className="btn"
					>
						¡Sitio Web!
					</a>
				</div>
			)}
		</div>
	);
};

export default Card;
