import React from "react";
import "./Home.css";
import { Link } from "react-scroll";
import { useLocation } from "react-router-dom";
import Usuarios from "../Usuarios/Usuarios";
import Navbar from "../Navbar/Navbar.jsx";

const Home = () => {
	const location = useLocation();
	return (
		<div>
			{location.pathname === "/" ? (
				<div className="home container">
					<div className="home-texto">
						<p className="title-home">¡Compra rifas con premios increíbles!</p>
						<p className="text">
							Descubre emocionantes oportunidades para ganar premios exclusivos.
							<br />
							¡Comienza tu viaje hacia la victoria hoy mismo!
						</p>
						<Link to="divRif" smooth={true} offset={0} duration={500}>
							<button className="btn">Ver rifas disponibles</button>
						</Link>
					</div>
				</div>
			) : (
				<div className="">
					<Navbar />
					<div className="home container home2">
						<div className="home-texto">
							<p className="title-home">
								¡Sé uno de nuestros afortunados ganadores!
							</p>
							<p className="text">
								¿Deseas un asombroso premio por un módico costo? <br />
								¡Participa de nuestras rifas como muchos han hecho!
							</p>
							<Link to="divCont" smooth={true} offset={0} duration={500}>
								<button className="btn">Ver compradores</button>
							</Link>
						</div>
					</div>
					<Usuarios />
				</div>
			)}
		</div>
	);
};

export default Home;
