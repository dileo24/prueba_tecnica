import React, { useEffect, useState } from "react";
import axios from "axios";
import "./Loterias.css";
import Card from "../Card/Card";
import { apiLoterias } from "../../services/loterias.services";

const Loterias = () => {
	const [loterias, setLoterias] = useState([]);

	useEffect(() => {
		const getLoterias = async () => {
			try {
				const response = await apiLoterias.getLoterias();
				setLoterias(response);
			} catch (error) {
				console.error("Error fetching loterias:", error);
			}
		};

		getLoterias();
	}, []);

	return (
		<div className="container divLot">
			<p className="title_sec">Loterías</p>
			<div className="loterias">
				{loterias &&
					loterias.map((loteria) => <Card key={loteria.id} data={loteria} />)}
			</div>
		</div>
	);
};

export default Loterias;
