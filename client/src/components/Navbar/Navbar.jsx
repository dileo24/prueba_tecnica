import React, { useState } from "react";
import "./Navbar.css";
import logo from "../../assets/logo.png";
import menuIcon from "../../assets/menu.png";
import { Link as ScrollLink } from "react-scroll";
import { Link as RouterLink, useLocation } from "react-router-dom";

const Navbar = () => {
	const [menu, setMenu] = useState(false);
	const toggleMenu = () => {
		menu ? setMenu(false) : setMenu(true);
	};
	const location = useLocation();
	return (
		<nav className="container">
			<RouterLink to="/">
				<div className="logoDiv">
					<img src={logo} alt="logo" className="logo" />
					<p className="logoP">Rifas Online</p>
				</div>
			</RouterLink>
			<ul className={menu ? "" : "hideMenu"}>
				{location.pathname === "/crear_rifa" ||
				location.pathname === "/usuarios" ? (
					<li>
						<RouterLink to="/">Inicio</RouterLink>
					</li>
				) : (
					<>
						<li>
							<ScrollLink to="home" smooth={true} offset={0} duration={500}>
								Inicio
							</ScrollLink>
						</li>
						<li>
							<ScrollLink to="divRif" smooth={true} offset={0} duration={500}>
								Rifas
							</ScrollLink>
						</li>
						<li>
							<ScrollLink to="divLot" smooth={true} offset={0} duration={500}>
								Loterías
							</ScrollLink>
						</li>
						<li>
							<RouterLink to="/usuarios">Usuarios</RouterLink>
						</li>
					</>
				)}
			</ul>
			<img
				src={menuIcon}
				alt="menuIcon"
				className="menuIcon"
				onClick={toggleMenu}
			/>
		</nav>
	);
};

export default Navbar;
