import React, { useEffect, useState } from "react";
import "./Modal.css";
import { apiRifas } from '../../services/rifas.services';
import { apiUsuarios } from '../../services/usuarios.services';

const Modal = ({ abrirModal, handleCerrar, rifa, DatosForm }) => {
  const [formData, setFormData] = useState({});
  const [precioTotal, setPrecioTotal] = useState(0);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

    if (name === "cantidadRifas" && !isNaN(value)) {
      const cantidad = parseInt(value);
      const precioUnitario = parseInt(rifa.valor);
      const total = cantidad * precioUnitario;
      setPrecioTotal(total);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const usuarioResponse = await apiUsuarios.createUsuario(formData);
      const usuarioId = usuarioResponse.newUsuario.id;

      await apiRifas.updateRifa( rifa.id, { usuarioId: usuarioId } )
      handleCerrar();
      window.location.reload();
    } catch (error) {
      console.error("Error al enviar los datos del formulario:", error);
    }
  };

  const handleClickAfuera = (e) => {
    if (e.target.classList.contains("modal")) {
      handleCerrar();
    }
  };

  useEffect(() => {
    if (abrirModal) {
      document.body.style.overflow = "hidden"; // deshabilitar scroll trasero
      document.addEventListener("click", handleClickAfuera);
    } else {
      document.body.style.overflow = "auto"; // habilitar scroll
      document.removeEventListener("click", handleClickAfuera);
    }
  }, [abrirModal]);

  if (!abrirModal) {
    return null;
  }

  const datosRifa = [
    { label: "Descripción", value: rifa.descripcion },
    { label: "Precio", value: rifa.valor },
    { label: "Fecha inicial", value: rifa.fecha_inicial },
    { label: "Fecha final", value: rifa.fecha_final },
    { label: "Lotería", value: rifa.loteria.nombre },
  ];

  return (
    <div className="modal" tabIndex="1">
      <div className="contenedor">
        <div className="divTitulo">
          <p className="title">Compra: {rifa.nombre}</p>
          <button type="button" className="btn-cerrar" onClick={handleCerrar}>
            X
          </button>
        </div>
        <div className="divDetalle">
          <div className="detalle">
            {datosRifa.map((detail, index) => (
              <p key={index}>
                <strong>{detail.label}:</strong> {detail.value}
                {detail.label === "Precio" && " u$d"}
              </p>
            ))}
          </div>
          <div className="form">
            <form onSubmit={handleSubmit}>
              {DatosForm.map((dato, index) => (
                <div key={index} className="inputDiv">
                  <label className="form-label">{dato.label}</label>
                  <input
                    type={dato.type}
                    className="form-input"
                    name={dato.name}
                    value={formData[dato.name] || ""}
                    onChange={handleChange}
                    required
                  />
                </div>
              ))}
              {DatosForm.some((dato) => dato.name === "cantidadRifas") && (
                <h5>Precio total: {precioTotal} u$d</h5>
              )}
              <button type="submit" className="btn enviar">
                Enviar
              </button>
            </form>
          </div>
        </div>
        <div className="buttons">
          <button type="button" className="btn cancelar" onClick={handleCerrar}>
            Cancelar
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
