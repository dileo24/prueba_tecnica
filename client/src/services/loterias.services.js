import axios from 'axios';

const baseURL = 'http://localhost:3001';

export const apiLoterias = {
  async getLoterias() {
    try {
      const response = await axios.get(`${baseURL}/loterias`);
      return response.data;
    } catch (error) {
      throw new Error(`Error al listar loterias: ${error}`);
    }
  },

  async createLoteria(formData) {
    try {
      const response = await axios.post(`${baseURL}/loterias`, formData);
      return response.data;
    } catch (error) {
      throw new Error(`Error al crear Loteria: ${error}`);
    }
  },
};
