import axios from 'axios';

const baseURL = 'http://localhost:3001';

export const apiUsuarios = {
  async getUsuarios() {
    try {
      const response = await axios.get(`${baseURL}/usuarios`);
      return response.data;
    } catch (error) {
      throw new Error(`Error al listar usuarios: ${error}`);
    }
  },

  async createUsuario(formData) {
    try {
      const response = await axios.post(`${baseURL}/usuarios`, formData);
      return response.data;
    } catch (error) {
      throw new Error(`Error al crear usuario: ${error}`);
    }
  },
};
