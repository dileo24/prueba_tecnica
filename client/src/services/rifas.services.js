import axios from 'axios';

const baseURL = 'http://localhost:3001';

export const apiRifas = {
  async getRifas() {
    try {
      const response = await axios.get(`${baseURL}/rifas`);
      return response.data;
    } catch (error) {
      throw new Error(`Error al listar rifas: ${error}`);
    }
  },

  async createRifa(formData) {
    try {
      const response = await axios.post(`${baseURL}/rifas`, formData);
      return response.data;
    } catch (error) {
      throw new Error(`Error al crear rifa: ${error}`);
    }
  },

  async updateRifa(id, formData) {
    try {
      const response = await axios.put(`${baseURL}/rifas/${id}`, formData);
      return response.data;
    } catch (error) {
      throw new Error(`Error al modificar rifa: ${error}`);
    }
  },

  async deleteRifa(id) {
    try {
      await axios.delete(`${baseURL}/rifas/${id}`);
    } catch (error) {
      throw new Error(`Error al borrar rifa: ${error}`);
    }
  }
};
