import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Home from "./components/Inicio/Home";
import Rifas from "./components/Rifas/Rifas";
import Loterias from "./components/Loterias/Loterias";
import Usuarios from "./components/Usuarios/Usuarios";
import CrearRifa from "./components/CrearRifa/CrearRifa";

const App = () => {
	return (
		<Router>
			<Routes>
				<Route path="/" element={<Main />} />
				<Route path="/crear_rifa" element={<CrearRifa />} />
				<Route path="/usuarios" element={<Home />} />
			</Routes>
		</Router>
	);
};

const Main = () => {
	return (
		<div>
			<Navbar />
			<Home />
			<Rifas />
			<Loterias />
		</div>
	);
};

export default App;
